/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.simpleprefs

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager

class HomeMotor(application: Application) : AndroidViewModel(application) {
  private val listener =
    SharedPreferences.OnSharedPreferenceChangeListener { _, _ -> emitState() }
  private val prefs = PreferenceManager.getDefaultSharedPreferences(application)
  private val _states = MutableLiveData<HomeViewState>()
  val states: LiveData<HomeViewState> = _states

  init {
    prefs.registerOnSharedPreferenceChangeListener(listener)
    emitState()
  }

  override fun onCleared() {
    prefs.unregisterOnSharedPreferenceChangeListener(listener)
  }

  private fun emitState() {
    _states.value = HomeViewState(
      prefs.getBoolean("checkbox", false),
      prefs.getString("field", "") ?: "",
      prefs.getString("list", "") ?: ""
    )
  }
}
